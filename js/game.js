setInterval(() => {
    const gameOver = document.getElementById("game-over-container")
    const mario = document.getElementById("mario")
    const cano = document.getElementById("cano")
    const nuvem = document.getElementById("nuvens")
    const marioPosition = window.getComputedStyle(mario).bottom.replace('px', '')
    
    if (cano.offsetLeft <= 145 && marioPosition <= 80) {
        if(cano.offsetLeft >= mario.offsetLeft) {
            nuvem.style.left = nuvem.offsetLeft + "px"
            nuvem.style.animation = "none";
            cano.style.left = cano.offsetLeft + "px"
            cano.style.animation = "none";
            gameOver.style.display = "block";
        }
    }

}, 5)

window.addEventListener("keydown", () => {
    const mario = document.getElementById("mario")
    mario.classList.add("pulo")

    setTimeout(() => {
        mario.classList.remove("pulo")
    }, 800)
})